import { HeartSubType, PickupVariant } from "isaac-typescript-definitions";
import {
  CollectibleType,
  PlayerType,
} from "isaac-typescript-definitions/dist/src/enums/collections/subTypes";
import { ModCallbackCustom, upgradeMod } from "isaacscript-common";

const MOD_NAME = "lost-holy-cloak";

main();

function main() {
  const mod = upgradeMod(RegisterMod(MOD_NAME, 1));

  mod.AddCallbackCustom(
    ModCallbackCustom.POST_PICKUP_COLLECT,
    postPickupCollect,
    PickupVariant.HEART,
    HeartSubType.SOUL,
  );
}

function postPickupCollect(_: EntityPickup, player: EntityPlayer) {
  const isLostCharacter = player.GetPlayerType() === PlayerType.LOST;
  const effects = player.GetEffects();
  const shieldsDown =
    effects.GetCollectibleEffect(CollectibleType.HOLY_MANTLE) === undefined;

  if (isLostCharacter && shieldsDown) {
    effects.AddCollectibleEffect(CollectibleType.HOLY_MANTLE);
  }

  return undefined;
}
